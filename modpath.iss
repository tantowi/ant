// ----------------------------------------------------------------------------
//
// Inno Setup Ver:	5.4.2
// Script Version:	1.4.1
// Author:			Jared Breland <jbreland@legroom.net>
// Homepage:		http://www.legroom.net/software
// License:			GNU Lesser General Public License (LGPL), version 3
//						http://www.gnu.org/licenses/lgpl.html
//
// Script Function:
//	Allow modification of environmental path directly from Inno Setup installers
//
// Instructions:
//	Copy modpath.iss to the same directory as your setup script
//
//	Add this statement to your [Setup] section
//		ChangesEnvironment=true
//
//	Add this statement to your [Tasks] section
//	You can change the Description or Flags
//	You can change the Name, but it must match the ModPathName setting below
//		Name: modifypath; Description: &Add application directory to your environmental path; Flags: unchecked
//
//	Add the following to the end of your [Code] section
//	ModPathName defines the name of the task defined above
//	ModPathType defines whether the 'user' or 'system' path will be modified;
//		this will default to user if anything other than system is set
//	setArrayLength must specify the total number of dirs to be added
//	Result[0] contains first directory, Result[1] contains second, etc.
//		const
//			ModPathName = 'modifypath';
//			ModPathType = 'user';
//
//		function ModPathDir(): TArrayOfString;
//		begin
//			setArrayLength(Result, 1);
//			Result[0] := ExpandConstant('{app}');
//		end;
//		#include "modpath.iss"
// ----------------------------------------------------------------------------

[Code]

//--------------------------------------------------------------------------------
// Split a string into an array using ';' as delimiter
//--------------------------------------------------------------------------------
procedure PExplode(var Dest: TStringList; Text: String);
var
	N : integer;
begin
  Dest.Clear();
	repeat
    N := Pos(';', Text);
		if (N > 0) then	
    begin
			Dest.Append (Copy(Text, 1, N-1));
			Text := Copy(Text, N+1, Length(Text));
		end else begin
			 Dest.Append (Text);
			 Text := '';
		end;
	until Length(Text)=0;
end;

//--------------------------------------------------------------------------------
// Find Text in array. If found return index on array, if not found return -1
//--------------------------------------------------------------------------------
function PFind (src: TStringList; Text: String) : integer;
var
  i : integer;
begin
  Text := Uppercase(Text);
  for i:= 0 to src.Count-1 do 
  begin
    if (Uppercase(src[i]) = Text) then 
    begin
      Result := i;
      Exit;
    end;
  end;
  Result := -1;
end;


//--------------------------------------------------------------------------------
// Find Text in array. If found return index on array, if not found return -1
//--------------------------------------------------------------------------------
function PBuild (src: TStringList) : string;
var
  i : integer;
  rs : string;
begin
  rs := '';
  for i:= 0 to src.Count-1 do 
  begin
    rs := rs + src[i] + ';';
  end;
  rs := Copy(rs, 1, Length(rs)-1);
  Result := rs;
end;


//--------------------------------------------------------------------------------
// AddSystemPath
//--------------------------------------------------------------------------------
procedure AddSystemPath(APath:string);
var
	regroot :	Integer;
	regpath :	String;
	PathStr :	String;
  Paths   : TStringList;
  N       : integer;

begin
	if (not UsingWinNT()) then Exit;

  paths   := TStringList.Create();
	regroot := HKEY_LOCAL_MACHINE;
	regpath := 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment';
	RegQueryStringValue(regroot, regpath, 'Path', PathStr);
  //MsgBox('Old: ' + PathStr, mbInformation, MB_OK);

  PExplode (Paths, PathStr);
  N := PFind (Paths, APath)
  if (N = -1) then Paths.Append (APath);

  PathStr := PBuild (Paths);
  //MsgBox('New: ' + PathStr, mbInformation, MB_OK);
	RegWriteStringValue(regroot, regpath, 'Path', PathStr);

end;


//--------------------------------------------------------------------------------
// RemoveSystemPath
//--------------------------------------------------------------------------------
procedure RemoveSystemPath(APath:string);
var
	regroot :	Integer;
	regpath :	String;
	PathStr :	String;
  Paths   : TStringList;
  N       : integer;

begin
	if (not UsingWinNT()) then Exit;

  paths   := TStringList.Create();
	regroot := HKEY_LOCAL_MACHINE;
	regpath := 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment';
	RegQueryStringValue(regroot, regpath, 'Path', PathStr);
  //MsgBox('Old: ' + PathStr, mbInformation, MB_OK);

  PExplode (Paths, PathStr);
  N := PFind (Paths, APath)
  if (N >= 0) then Paths.Delete(N);

  PathStr := PBuild (Paths);
  //MsgBox('New: ' + PathStr, mbInformation, MB_OK);
	RegWriteStringValue(regroot, regpath, 'Path', PathStr);

end;


