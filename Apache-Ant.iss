; Apache Ant Win32
; Installer created by Tantowi Mustofa
; ttw@tantowi.com

#define MyAppName "Apache Ant"
#define MyAppVer "1.10.1"

[Setup]
AppID=COM.TANTOWI.APACHEANT
AppName={#MyAppName}
AppVersion={#MyAppVer}
AppVerName={#MyAppName} {#MyAppVer}

AppPublisher=Apache Ant Project
AppPublisherURL=https://ant.apache.org

; Support dialog on add/remove program
AppContact=Tantowi Mustofa
AppComments=Apache Ant Setup by Tantowi Mustofa, ttw@tantowi.com
AppSupportPhone=+62-817-333155
AppSupportURL=https://www.tantowi.com/ant
AppUpdatesURL=https://www.tantowi.com/ant

; Setup Default
OutputBaseFileName={#MyAppName} {#MyAppVer} Setup
DefaultDirName={code:GetProgramFiles}\Apache\Ant\
DefaultGroupName=Apache Ant
OutputDir=.
SetupIconFile=ant_icon.ico

UninstallDisplayIcon={app}\ant_icon.ico
ChangesEnvironment=yes
;ChangesAssociations=yes
;AlwaysRestart=yes

DisableStartupPrompt=yes
DisableDirPage=yes
DisableProgramGroupPage=yes
DisableReadyMemo=yes

;compression=none
PrivilegesRequired=admin


[Icons]
Name: "{group}\{#MyAppName} Documentation"; Filename: "{app}\manual\index.html";

[Files]
Source: "apache-ant-{#MyAppVer}\*"; DestDir: "{app}"; Flags: recursesubdirs
Source: "ant_icon.ico"; DestDir: "{app}"


[Registry]
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment";ValueType: string; ValueName: "ANT_HOME"; ValueData: "{app}"; Flags: uninsdeletevalue

[Messages]
SetupWindowTitle=%1 Setup
ExitSetupMessage=The installation will be aborted. Exit anyway?

WizardReady={#MyAppName} {#MyAppVer} Setup Wizard
ReadyLabel1=
ReadyLabel2b=Apache Ant is a Java-based build tool.%nThis version of Apache Ant require Java 8 at runtime%n%n%n%n%n%n%n%n%n%n%n%n Apache Ant� is trademark of The Apache Software Foundation.%n%n This setup is Copyright � 2017 Tantowi Mustofa.%n www.tantowi.com%n




[Code]

#include "modpath.iss"


// Get "Program Files" folder, both in 32bit and in 64bit
function GetProgramFiles(Param: string): string;
begin
   if IsWin64 then Result := ExpandConstant('{pf64}')
   else Result := ExpandConstant('{pf32}')
end;


//
// This function called when install success
//
function PrepareToInstall(var NeedsRestart: Boolean): String;
begin
  AddSystemPath(ExpandConstant('{app}\bin'));
  Result := '';
end;


//
// This function called when starting uninstall
//
procedure InitializeUninstallProgressForm();
begin
  RemoveSystemPath(ExpandConstant('{app}\bin'));
end;


